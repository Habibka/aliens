import EventEmitter from 'eventemitter3';
import gsap from 'gsap/all';

export default class Cow extends EventEmitter {
  constructor() {
    super();
    this._cowElement = '.cow';
  }
  static get events() {
    return {
      ABDUCT_COMPLETED: 'abduct_completed',
    };
  }
  async moveTo() {
    await gsap.to(this._cowElement, { y: -390, duration: 2, id: 'cowAbduction' });
    this.emit(Cow.events.ABDUCT_COMPLETED);
  }

  async hide() {
    await gsap.to(this._cowElement, { duration: 0, opacity: 0, id: 'cowHide' });
  }
}
