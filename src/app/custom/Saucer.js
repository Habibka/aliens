import gsap from 'gsap/all';
import EventEmitter from 'eventemitter3';

export default class Saucer extends EventEmitter {
  constructor() {
    super();
    this._saucerElement = '.ufo';
    this._beamTopElement = '#beam-top';
    this._beamBottomElement = '#beam-bottom';

  }

  static get events() {
    return {
      FLY_IN: 'fly_in',
      FLY_AWAY: 'fly_away',
      BEAM_SHOW: 'beam_showed',
      BEAM_HIDE: 'beam_hide',
    };
  }

  async moveTo() {
    await gsap.to(this._saucerElement, { x: -835, ease: "power2.inOut", duration: 3, id: 'flyIn' });

    this.emit(Saucer.events.FLY_IN);
  }

  async toggleBeam(isBeamVisible) {
    const opacity = isBeamVisible ? 0.6 : 0;
    const time = isBeamVisible ? 1 : 0;
    const topBeam = isBeamVisible ? 'showTopBeam' : 'hideTopBeam';
    const bottomBeam = isBeamVisible ? 'showBottomBeam' : 'hideBottomBeam';

    const animation = gsap.timeline({duration: time });

    await animation.to(this._beamTopElement, { opacity, id: topBeam })
      .to(this._beamBottomElement, { opacity, id: bottomBeam }, '<');

    this.test(isBeamVisible);
  }

  test(isBeamVisible) {
    isBeamVisible ? this.emit(Saucer.events.BEAM_SHOW) : this.emit(Saucer.events.BEAM_HIDE);
  }

  async flyaway() {
    await gsap.to(this._saucerElement, { x: -1800, ease: 'power2.inOut', duration: 3, id: 'flyOut' });
    this.emit(Saucer.events.FLY_AWAY);
  }
}
