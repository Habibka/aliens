import Cow from './Cow';
import Saucer from './Saucer';

export default class Animation {
  constructor() {
    this.saucer = new Saucer();
    this.cow = new Cow();

    this.saucer.on(Saucer.events.FLY_IN, () => {
      this.saucer.toggleBeam(true);
    });
    this.saucer.on(Saucer.events.BEAM_SHOW, () => {
      this.cow.moveTo();
    });
    this.cow.on(Cow.events.ABDUCT_COMPLETED, () => {
      this.cow.hide();
      this.saucer.toggleBeam(false);
    });
    this.saucer.on(Saucer.events.BEAM_HIDE, () => {
      this.saucer.flyaway();
    });
    this.start();
  }

  start() {
    this.saucer.moveTo();
  }
}
